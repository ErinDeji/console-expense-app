<?php
spl_autoload_register();

/**
 * Menu function for running all the application method
 * And for accepting all inputs
 */

function menuFunction()
{
    echo "\n\n\e[0;41m     WELCOME TO MR JOHN'S EXPENSES      \e[0m\n";
    echo "\e[0;31m    *********        *********\n";
    echo "\e[0;31m         *****      *****\n";
    echo "\e[0;32mPress 1: To Create Expense Group\e[0m\n";
    echo "\e[0;32mPress 2: To Update Expense Group\e[0m\n";
    echo "\e[0;32mPress 3: To Show All Expense Groups\e[0m\n";
    echo "\e[0;32mPress 4: To Remove Expense Group\e[0m\n";
    echo "\e[0;32mPress 5: To Create Expense\e[0m\n";
    echo "\e[0;32mPress 6: To Update An Expense\e[0m\n";
    echo "\e[0;32mPress 7: To Delete An Expense\e[0m\n";
    echo "\e[0;32mPress 8: To Show All Expenses\e[0m\n";
    echo "\e[0;32mPress 9: To Show Percentage Amount Saved\e[0m\n";
    echo "\e[0;32mPress 10: To Show Amount Spent\e[0m\n";
    echo "\e[0;31mPress 11: Quit\e[0m\n";
}
$userInput = new User;
$expenseObj = new ExpenseItem($userInput);
$handler = new Handler;
while (true) {
    menuFunction();
    $input = readline("\e[0;32mEnter input here: \e[0m");

    switch ($input) {
        case 1: {
                echo "\n\e[0;32m Enter Expense Group Name ( Must be unique) :: \e[0m";
                $input = $userInput->getUserOption();
                $input = strtolower($input);

                $status = $expenseObj->createExpenseGroup($input);
                is_array($status) ? $handler->createdSuccesfully() : $handler->alreadyExist();

                break;
            }
        case 2: {
                $handler->changeFrom();
                $oldGroupName = $userInput->getUserOption();
                $oldGroupName = strtolower($oldGroupName);

                $handler->changeTo();
                $newGroupName = $userInput->getUserOption();
                $newGroupName = strtolower($newGroupName);

                $status = $expenseObj->updateExpenseGroup($oldGroupName, $newGroupName);

                (($status)) ? $handler->expenseUpdated() :  $handler->doesNotExist();

                break;
            }

        case 3: {
                $newArr = $expenseObj->showAllExpenseGroups();
                foreach ($newArr as $key => $value) {
                    echo "\n\e[0;36m[ GroupName: ( " . $key . " ) ]\e[0m\n";
                }
                break;
            }
        case 4: {
                $newArr = $expenseObj->showAllExpenseGroups();
                echo "\n\e[0;36mList of groups to delete :: \e[0m";
                foreach ($newArr as $key => $value) {
                    echo "\e[0;33m[ " . $key . " ], \e[0m";
                }
                echo "\n\n\e[0;32mEnter the group you want to delete ( Must be unique):: \e[0m";
                $input = $userInput->getUserOption();
                $input = strtolower($input);
                $status = $expenseObj->removeExpenseGroup($input);
                (isset($status)) ? $handler->doesNotExist() : $handler->deletedSuccesfully();
                break;
            }
        case 5: {
                echo "\n\e[0;32m===== Create new Expense ===== \e[0m\n";
                $newArr = $expenseObj->arrExpenseGroup;
                echo "\n\e[0;36mList of group(s) available :: \e[0m";
                foreach ($newArr as $key => $value) {
                    echo "\e[0;33m[ " . $key . " ], \e[0m";
                }
                echo "\n\n\e[0;32mSelect Group to create Expense in :\e[0m";
                $expenseGroupName = $userInput->getUserOption();
                $expenseGroupName = strtolower($expenseGroupName);
                if (!isset($expenseObj->arrExpenseGroup[$expenseGroupName])) {
                    echo "\n\e[0;31m$expenseGroupName does not exist... Select from groups listed above!\e[0m\n";
                } else {

                    $handler->title();
                    $title = $userInput->getUserOption();

                    $handler->benefactor();
                    $benefactor = $userInput->getUserOption();

                    $handler->percentage();
                    $percentage = $userInput->getUserOption();

                    $status = $expenseObj->createExpense($expenseGroupName, $title, $percentage, $benefactor);
                    (($status)) ? $handler->expenseCreated() :  $handler->expenseError2();
                }
                break;
            }
        case 6: {
                $newArr = $expenseObj->arrExpenseGroup;
                echo "\n\e[0;36mList of group(s) to update from :: \e[0m";
                foreach ($newArr as $key => $value) {
                    echo "\e[0;33m[ " . $key . " ], \e[0m";
                }
                echo "\n\n\e[0;32mSelect Group to update Expense from :\e[0m";
                $groupName = $userInput->getUserOption();
                $groupName = strtolower($groupName);

                if (!isset($expenseObj->arrExpenseGroup[$groupName])) {
                    echo "\n\e[0;31m$groupName does not exist... Select from groups listed above!\e[0m\n";
                } else {
                    echo "\n\e[0;36mList of Expense Title(s) to update from :: \e[0m";
                    foreach ($expenseObj->arrExpenseGroup[$groupName] as $key => $value) {
                        echo "\e[0;33m[ " . $value['title'] . " ], \e[0m";
                    }

                    echo "\n\n\e[0;33mEnter Title of Expense :\e[0m\n";
                    $title = $userInput->getUserOption();

                    $handler->newtitle();
                    $newExpenseTitle = $userInput->getUserOption();

                    $handler->newBenefactor();
                    $benefactor = $userInput->getUserOption();

                    $handler->newPercentage();
                    $percentage = $userInput->getUserOption();

                    $status = $expenseObj->updateAnExpense($groupName, $title, $percentage, $benefactor, $newExpenseTitle);
                    (($status)) ? $handler->expenseUpdated() :  $handler->expenseError2();
                }
                break;
            }
        case 7: {
                $newArr = $expenseObj->arrExpenseGroup;
                echo "\n\e[0;36mList of groups to delete from :: \e[0m";
                foreach ($newArr as $key => $value) {
                    echo "\e[0;33m[ " . $key . " ], \e[0m";
                }
                echo "\n\n\e[0;33mSelect Group to delete Expense from : \e[0m";
                $groupName = $userInput->getUserOption();
                $groupName = strtolower($groupName);

                if (!isset($expenseObj->arrExpenseGroup[$groupName])) {
                    echo "\n\e[0;31m$groupName does not exist... Select from groups listed above! \e[0m\n";
                } else {
                    echo "\n\e[0;36mList of Expense Title(s) to delete from :: \e[0m";
                    foreach ($expenseObj->arrExpenseGroup[$groupName] as $key => $value) {
                        echo "\e[0;33m[ " . $value['title'] . " ], \e[0m";
                    }

                    echo "\n\n\e[0;33mEnter Title you want to delete here :: \e[0m";
                    $title = $userInput->getUserOption();
                    $title = strtolower($title);

                    $status = $expenseObj->deleteAnExpense($groupName, $title);

                    $status ? $handler->deletedSuccesfully() : $handler->doesNotExist();
                }
                break;
            }
        case 8: {
                $newArr = $expenseObj->showAllExpenseGroups();
                foreach ($newArr as $key => $value) {
                    echo "\n\e[0;36m[ GroupName: ( " . $key . " ) ]\e[0m\n";
                    foreach ($value as $expenseGroupName => $expense) {
                        echo "\n";
                        foreach ($expense as $newKey => $newValue) {
                            echo $newKey . "\e[0;36m : \e[0m" . $newValue . PHP_EOL;
                        }
                    }
                }
                break;
            }
        case 9: {
                echo PHP_EOL . $expenseObj->showPercentageAmountSaved() . "\e[0;33m$ was saved out of released money for spending \e[0m\n";
                break;
            }
        case 10: {
                echo  "\n\e[0;33m The total amount spent from benefactors is " . $expenseObj->showAmountSpent() . "$ out from amount saved for spending \e[0m\n";
                break;
            }
        case 11: {
                echo "\n\e[0;33m Have a nice day Bye Bye!\e[0m\n";
                $expenseObj->Quit();
                break;
            }
        default: {
                echo "\n\n\e[0;31mNo / Wrong choice entered. Please provide a valid input.\e[0m\n\n";
            }
    }
}