<?php
spl_autoload_register();

/**
 * ExpenseItem Class
 */
class ExpenseItem extends ExpenseGroup
{
    public $totalExpensePercentage = 0;
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /** 
     * @param mixed $groupName
     * @param mixed $strTitle
     * get the key of Expense using array search and array 
     *column. it will loop through titles in array to find match
     */
    public function getKeyFromExpense($groupName, $strTitle)
    {
        // get the key of Expense using title
        return array_search($strTitle, array_column($this->arrExpenseGroup[$groupName], 'title'));
    }


    /**
     * This method creates an expense
     * @return arrExpenseGroup
     * @param mixed $expenseGroupName
     * @param mixed $title
     * @param mixed $percentage
     * @param mixed $benefactor
     * @isset Check if group exist.
     */
    public function createExpense($expenseGroupName, $title, $percentage, $benefactor)
    {
        // check if group exist or does not
        if (isset($this->arrExpenseGroup[$expenseGroupName])) {
            // adding Expense
            // checking if title exist or does not
            $key = $this->getKeyFromExpense($expenseGroupName, $title);

            // if it exist 
            if (is_numeric($key)) {
                return false;
            }

            if (is_numeric($percentage)) {
                // check if percentage exceed limit or does not
                if (($this->totalExpensePercentage + $percentage) > ($this->user->minimumPercentage)) {
                    return false;
                } else {
                    // array for Expense
                    $arrNewExpense = array(
                        'title' => $title,
                        'benefactor' => $benefactor,
                        'percentage' => $percentage
                    );

                    //update precentage
                    $this->totalExpensePercentage += $percentage;

                    $this->arrExpenseGroup[$expenseGroupName][] = $arrNewExpense;
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Update expense Method
     * @param mixed $expenseGroupName  
     *@param mixed $title
     *@param mixed $percentage
     *@param mixed $benefactor
     *@param mixed $newExpenseTitle
     *@return new ArrayExpense
     */
    public function updateAnExpense($expenseGroupName, $title, $percentage, $benefactor, $newExpenseTitle)
    {
        // check group exist or not
        if (isset($this->arrExpenseGroup[$expenseGroupName])) {
            $key = $this->getKeyFromExpense($expenseGroupName, $title);
            // title checking
            if (is_numeric($key)) {
                $arrNewExpense = array(
                    'title' => $newExpenseTitle,
                    'benefactor' => $benefactor,
                    'percentage' => $percentage
                );
                $existingPercentage = $this->arrExpenseGroup[$expenseGroupName][$key]['percentage'];
                //dump data

                $arrOldExpense = $this->arrExpenseGroup[$expenseGroupName][$key];

                if (($this->totalExpensePercentage - $existingPercentage + $percentage) > ($this->user->minimumPercentage)) {
                    return false;
                } else {
                    // update modified Expense
                    $this->arrExpenseGroup[$expenseGroupName][$key] = $arrNewExpense;

                    //update percentage - remove old percentage
                    $this->totalExpensePercentage -= $arrOldExpense['percentage'];

                    //update percentage - add new percentage
                    $this->totalExpensePercentage += $percentage;
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * This method shows Percentage Amount Saved
     * @return monthly - minimum - total pecentage
     */
    public function showPercentageAmountSaved()
    {
        $perSaved = $this->user->monthlyIncome - $this->user->minimumPercentage;
        return $perSaved - $this->totalExpensePercentage;
    }


    /**
     * This method shows amount spent from released money
     * @return totalExpensePercentage
     */
    public function showAmountSpent()
    {
        return $this->totalExpensePercentage;
    }


    /**
     *@param mixed $expenseGroupName
     *@param mixed $title
     * This method deletes an expense
     * @return new arrExpenseGroup
     */
    // delete an expense from a group
    public function deleteAnExpense($expenseGroupName, $title)
    {
        // check group exist or not
        if (isset($this->arrExpenseGroup[$expenseGroupName])) {

            $key = $this->getKeyFromExpense($expenseGroupName, $title);

            // check if title exist or not
            if (!is_numeric($key)) {
                return false;
            } else {
                $this->totalExpensePercentage -= $this->arrExpenseGroup[$expenseGroupName][$key]['percentage'];
                // remove Expense
                unset($this->arrExpenseGroup[$expenseGroupName][$key]);
                return true;
            } // ****
        } else {
            return false;
        }
    }


    /**
     * This method quits the application
     * @return die
     */
    public function quit()
    {
        exit;
    }
}