<?php

/**
 * ExpenseGroup Class
 */
class ExpenseGroup

{
    public $arrExpenseGroup = array();

    /**
     * @return properties $user, $alertAndErrorMessage
     * using composition to access user class and alertAndErrorMessage class
     * */


    /**
     * This method creates an expense group
     * @return undeclared value
     * @isset Check if the value is declared. if it exist return null
     */
    public function createExpenseGroup($expenseGroupName)
    {
        if (isset($this->arrExpenseGroup[$expenseGroupName])) {
            return false;
        }
        $expenseArray = $this->arrExpenseGroup[$expenseGroupName] = array();
        return $expenseArray;
    }


    /**
     * This method shows expense group(s)
     * @return arrExpenseGroup
     */
    public function showAllExpenseGroups()
    {
        return $this->arrExpenseGroup;
    }


    /**
     * This method is used to remove expense group
     * @return arrExpenseGroup
     * @unset Check if the value is declared. if not return @removeEspenseGroup
     */
    public function removeExpenseGroup($expenseGroupName)
    {
        if (!isset($this->arrExpenseGroup[$expenseGroupName])) {
            return false;
        } else {
            unset($this->arrExpenseGroup[$expenseGroupName]);
        }
    }


    /**
     * This method is used to update a particular expense group
     * @return new arrExpenseGroup
     * @isset Check if the value is declared. if it isn't return null
     */
    public function updateExpenseGroup($oldGroupName, $newGroupName)
    {
        // check group exist or not
        if (isset($this->arrExpenseGroup[$oldGroupName]) || isset($this->arrExpenseGroup[$newGroupName])) {
            $arrExpenseGroupData = $this->arrExpenseGroup[$oldGroupName];

            // assign dumped expense data into new group
            $this->arrExpenseGroup[$newGroupName] = $arrExpenseGroupData;

            //unset old group
            unset($this->arrExpenseGroup[$oldGroupName]);
            return true;
        } else {
            return false;
        }

        // check group is duplicate or not

    }
}

//code writer
