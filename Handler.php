<?php

/**
 * Handles all error and alert feedbacks
 */
class Handler

{
    public function alreadyExist()
    {
        echo "\n\e[0;33mValue already exist... Please enter a new one \e[0m\n";
    }
    public function deletedSuccesfully()
    {
        echo "\n\e[0;31mRemoved Succesfully\e[0m\n ";
    }
    public function createdSuccesfully()
    {
        echo "\n\e[0;32mCreated Succesfully\e[0m\n ";
    }
    public function doesNotExist()
    {
        echo "\n\e[0;33mInvalid or value does not exist... Please a valid value \e[0m";
    }
    public function changeFrom()
    {
        echo "\n\e[0;33mChange From :: \e[0m\n";
    }
    public function changeTo()
    {
        echo "\n\e[0;33mChange To :: \e[0m\n";
    }
    public function title()
    {
        echo "\n\e[0;32mEnter Title : \e[0m\n";
    }
    public function benefactor()
    {
        echo "\n\e[0;32mEnter Benefactor : \e[0m\n";
    }
    public function percentage()
    {
        echo "\n\e[0;32mEnter Percentage (in numbers only) : \e[0m\n";
    }

    public function expenseCreated()
    {
        echo "\n\e[0;32m********** Expense Created ********** \e[0m\n";
    }
    public function expenseUpdated()
    {
        echo "\n\e[0;32mExpense Updated \e[0m\n";
    }
    public function titleModifyExpense()
    {
        echo "\n\e[0;32mEnter the expense title you want to modify :\e[0m\n";
    }

    public function doesNotExistInGroup()
    {
        echo "\n\e[0;33mTitle does not exist in the expense group specified \e[0m\n";
    }
    public function newtitle()
    {
        echo "\n\e[0;32mEnter a new Title : \e[0m\n";
    }
    public function newBenefactor()
    {
        echo "\n\e[0;32mEnter new Benefactor : \e[0m\n";
    }
    public function newPercentage()
    {
        echo "\n\e[0;32mMake sure percentage is (in numbers only) : \e[0m\n";
    }
    public function expenseError()
    {
        echo "\n\e[0;33mInvalid or value does not exist... Please enter a valid value \e[0m\n";
    }
    public function expenseError2()
    {
        echo "\e[0;33mInvalid Input!!!\e[0m\n";
        echo "\e[0;33mExeed percentage limit or Make sure percentage is in numbers only ??? \e[0m\n";
    }
    public function deleteTitle()
    {
        echo "\n\e[0;33mEnter Title of expense you want to delete : \e[0m\n";
    }
}