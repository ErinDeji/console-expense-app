<?php
spl_autoload_register();


class User
{
    public $monthlyIncome = 0;
    public $minimumPercentage = 0;


    // Trigger user wages information when initialized
    public function __construct()
    {
        $this->getPrimaryInfo();
    }


    // Geting initial info from user
    public function getPrimaryInfo()
    {
        echo "\n\e[0;41m     WELCOME TO MR JOHN'S EXPENSES      \e[0m\n";
        echo "\n\e[0;32m Enter Monthly Income :: \e[0m";
        $this->monthlyIncome = $this->getUserOption();
        echo "\n\e[0;32m Enter Minimun Percetage For Saving ::\e[0m";
        $this->minimumPercentage = $this->getUserOption();
    }


    // For handling Input 
    public static function getUserOption()
    {
        return trim(fgets(STDIN));
    }
}
